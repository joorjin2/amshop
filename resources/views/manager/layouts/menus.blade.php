
@foreach (session()->get('user_info')->access() as $menu)
    <li class="nav-item">
        <a href=" {{ route($menu->name) }} " class="nav-link @if(Request::url() == route($menu->name)) active @endif ">
            <i class="{{ $menu->icon }}"></i>
            <p>
                {{$menu->title}}
            </p>
        </a>
    </li>
@endforeach
