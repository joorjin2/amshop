  <!-- Main Footer -->
  {{-- <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>CopyLeft &copy; 2018 <a href="http://github.com/hesammousavi/">حسام موسوی</a>.</strong>
  </footer>
</div> --}}
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('LTE_master/plugins/jquery/jquery.min.js');}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('LTE_master/plugins/bootstrap/js/bootstrap.bundle.min.js');}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('LTE_master/dist/js/adminlte.min.js');}}"></script>

<!-- ChartJS 1.0.1 -->
<script src="{{ asset('LTE_master/plugins/chart.js/cdn.js') }}" ></script>

<script src="{{ asset('LTE_master/dist/js/persian-date.min.js') }}"></script>
<script src="{{ asset('LTE_master/dist/js/persian-datepicker.min.js') }}"></script>

<script src="{{ asset('LTE_master/plugins/JalaliDatePicker/static/js/app.js') }}"></script>
@yield('script')



</body>
</html>
