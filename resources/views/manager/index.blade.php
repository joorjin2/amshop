@extends('manager.layouts.master')
@section('title')
    صفحه اصلی
@endsection


@section('center')
    <div class="row">
        <div class="col-6">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">بازدید روزانه(30 روز)</h3>


                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="bazdidRpzaneh" style="height: 140px; width: 281px;" width="962" height="480"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">فرروش روزانه (30 روز)</h3>


                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="forooshRoozaneh" style="height: 140px; width: 281px;" width="962" height="480"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">بازدهی فروش (30 روزه)</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="bazdahiRozaneh" style="height: 280px; width: 281px;" width="962" height="280"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection



@section('script')
    <script>
        const ctx = document.getElementById('bazdidRpzaneh').getContext('2d');
        const bazdidRpzaneh = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                    26, 27, 28, 29, 30, 31
                ],
                datasets: [{
                    label: 'بازدید روزانه کل',
                    data: [2, 2, 3, 2, 4, 2, 5, 5, 4, 6, 7, 5, 8, 5, 9, 10, 9, 11, 5, 11, 12, 10, 13],
                    backgroundColor: [
                        '#4fa845',
                    ],
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });



        const ctxt = document.getElementById('forooshRoozaneh').getContext('2d');
        const forooshRoozaneh = new Chart(ctxt, {
            type: 'bar',
            data: {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                    26, 27, 28, 29, 30, 31
                ],
                datasets: [{
                    label: 'بازدید روزانه کل',
                    data: [10, 9, 11, 5, 11, 12, 10, 13, 2, 2, 3, 2, 4, 2, 5, 5, 4, 6, 7, 5, 8, 5, 9, ],
                    backgroundColor: [
                        '#0879fa',
                    ],
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });




        const ctxtt = document.getElementById('bazdahiRozaneh').getContext('2d');
        const bazdahiRozaneh = new Chart(ctxtt, {
            type: 'bar',
            data: {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                    26, 27, 28, 29, 30, 31
                ],
                datasets: [
                {
                    label: 'فروش روزانه کل',
                    data: [2, 2, 3, 2, 4, 2, 5, 5, 4, 6, 7, 5, 8, 5, 9, 10, 9, 11, 5, 11, 12, 10, 13],
                    backgroundColor: [
                        '#0879fa',
                    ],
                },
                {
                    label: 'بازدید روزانه کل',
                    data: [10, 9, 11, 5, 11, 12, 10, 13, 2, 2, 3, 2, 4, 2, 5, 5, 4, 6, 7, 5, 8, 5, 9, ],
                    backgroundColor: [
                        '#4fa845',
                    ],
                }
            ],

            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endsection
