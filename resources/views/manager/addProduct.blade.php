@extends('manager.layouts.master')
@section('title')
    افزودن محصول
@endsection
@section('script')
    {{-- مدیریت تصویر --}}
    <script>
        $("#fileUpload").on('change', function () {

            //Get count of selected files
            var countFiles = $(this)[0].files.length;

            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();

            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {

                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);
                        }

                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }

                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("png.jpg.jpeg only accept");
            }
        });
    </script>
@endsection
@section('center')

    @if(!empty($ok))
        <div class="alert alert-success">
            {{ $ok }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title"> افزودن محصول </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="" method="post" role="form" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="productTitle"> عنوان محصول </label>
                    <input type="text" class="form-control" name="productTitle" id="productTitle"
                           placeholder="عنوان محصول را وارد کنید" required>
                </div>
                <div class="form-group">
                    <label for="productBrand"> برند محصول </label>
                    <input type="text" class="form-control" name="productBrand" id="productBrand"
                           placeholder="برند محصول را وارد کنید" required>
                </div>
                <div class="form-group">
                    <label>دسته بندی محصول</label>
                    <select class="form-control select2" name="productCategory" style="width: 100%;" required>
                        <option selected="selected" disabled>دسته بندی</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->product_category }}">{{ $category->product_category }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            توضیحات محصول
                        </h3>
                        <!-- tools box -->
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                        <div class="mb-3">
                <textarea class="textarea" name="productDescription" placeholder="لطفا متن خود را وارد کنید"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                          required></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="productImage">عکس محصول </label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input imgblog" id="fileUpload" name="productImage[]" multiple>
                            <label class="custom-file-label" for="exampleInputFile">انتخاب عکس</label>
                        </div>
                    </div>
                    <div id="image-holder"></div>
                </div>
                <div class="form-group">
                    <label for="productKeywords"> کلمات کلیدی </label>
                    <input type="text" class="form-control" name="productKeywords" id="productKeywords"
                           placeholder=" کلمات کلیدی را وارد کنید" required>
                </div>
                <div class="form-group">
                    <label for="productPrice"> قیمت </label>
                    <input type="number" class="form-control" name="productPrice" id="productPrice"
                           placeholder="قیمت را وارد کنید" required>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">افزودن</button>
                </div>
            </div>
            <!-- /.card-body -->
@endsection
