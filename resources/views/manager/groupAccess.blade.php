@extends('manager.layouts.master')
@section('title')
    مدیریت دسترسی ها
@endsection

@section('center')

    @if(Session::get('ok'))
        <div class="alert alert-success">
            {{ Session::get('ok') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">افزودن دسترسی</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="" method="post" role="form">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label> گروه ها</label>
                    <select class="form-control select2" name="rank" id="addAccess_rank" style="width: 100%;" required>
                        <option selected="selected" disabled> انتخاب گروه</option>
                        @foreach($ranks as $rank)
                            <option value="{{ $rank->rank }}">{{ $rank->rank }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label> منو ها </label>
                    <select class="form-control select2" name="menu" id="addAccess_menu" style="width: 100%;" required>
                        <option selected="selected" disabled>یک گروه انتخاب کنید</option>
                    </select>
                </div>
                <div class="card-footer">
                    <button type="submit" name="access" value="add" class="btn btn-success">افزودن دسترسی</button>
                </div>
            </div>
        </form>
    </div>

    <br>

    <div class="card card-primary mt-3">
            <div class="card-header">
                <h3 class="card-title">حذف دسترسی</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label> گروه ها</label>
                        <select class="form-control select2" name="rank" id="deleteAccess_rank" style="width: 100%;" required>
                            <option selected="selected" disabled> انتخاب گروه</option>
                            @foreach($ranks as $rank)
                                <option value="{{ $rank->rank }}">{{ $rank->rank }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label> منو ها </label>
                        <select class="form-control select2" name="menu" id="deleteAccess_menu" style="width: 100%;" required>
                            <option selected="selected" disabled>یک گروه انتخاب کنید</option>
                        </select>
                    </div>
                    <div class="card-footer">
                        <button type="submit" name="access" value="delete" class="btn btn-danger">حذف دسترسی</button>
                    </div>
                </div>
            </form>
        </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#addAccess_rank').on('change', function () {
                var rank = $(this).val();
                if (rank) {
                    $.ajax({
                        url: '/getMenus',
                        type: "POST",
                        data: {
                            rank: rank,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function (data) {

                            if (data) {
                                $('#addAccess_menu').empty();
                                $('#addAccess_menu').append('<option hidden>انتخاب منو</option>');
                                var notAccess = data.notAccess;
                                $.each(notAccess, function (key,value) {
                                    console.log(value)
                                    $('select[id="addAccess_menu"]').append('<option value="' + value.id + '">' + value.title + '</option>');
                                });
                            } else {
                                $('#addAccess_menu').empty();
                            }
                        }
                    });
                } else {
                    $('#addAccess_menu').empty();
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#deleteAccess_rank').on('change', function () {
                var rank = $(this).val();
                if (rank) {
                    $.ajax({
                        url: '/getMenus',
                        type: "POST",
                        data: {
                            rank: rank,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function (data) {

                            if (data) {
                                $('#deleteAccess_menu').empty();
                                $('#deleteAccess_menu').append('<option hidden>انتخاب منو</option>');
                                var access = data.access;
                                $.each(access, function (key, value) {
                                    console.log(value)
                                    $('select[id="deleteAccess_menu"]').append('<option value="' + value.menu_id + '">' + value.title + '</option>');
                                });
                            } else {
                                $('#deleteAccess_menu').empty();
                            }
                        }
                    });
                } else {
                    $('#deleteAccess_menu').empty();
                }
            });
        });
    </script>
@endsection
