@extends('manager.layouts.master')
@section('title')
    افزودن منو
@endsection

@section('center')

    @if(!empty($ok))
        <div class="alert alert-success">
            {{ $ok }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title"> افزودن منو </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="" method="post" role="form">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="menuTitle"> عنوان منو </label>
                    <input type="text" class="form-control" name="menuTitle" id="menuTitle"
                           placeholder="عنوان منو را وارد کنید" value="{{ old('menuTitle') }}" required>
                </div>
                <div class="form-group">
                    <label for="menuIcon"> ایکون منو </label>
                    <input type="text" class="form-control" name="menuIcon" id="menuIcon"
                           placeholder="ایکون منو را وارد کنید" value="{{ old('menuIcon') }}" required>
                </div>
                <div class="form-group">
                    <label for="underMenuId"> ایدی زیر منو </label>
                    <input type="text" class="form-control" name="underMenuId" id="underMenuId"
                           placeholder=" ایدی زیر منو را وارد کنید" value="{{ old('underMenuId') }}">
                </div>
                <div class="form-group">
                    <label for="menuName"> نام منو </label>
                    <input type="text" class="form-control" name="menuName" id="menuName"
                           placeholder="نام منو را وارد کنید" value="{{ old('menuName') }}">
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">افزودن</button>
                </div>
            </div>
            <!-- /.card-body -->
@endsection
