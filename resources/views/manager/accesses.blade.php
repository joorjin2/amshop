@extends('manager.layouts.master')
@section('title')
     مدیریت دسترسی ها
@endsection

@section('center')

    <div class="p-5 text-center">
        <a href="{{ route('groupAccess') }}" class="btn btn-info w-25">دسترسی گروه</a>
        <a href="{{ route('userAccess') }}" class="btn btn-info w-25">دسترسی کاربر</a>
    </div>

@endsection
