@extends('manager.layouts.master')
@section('title')
    مدیریت بلاگ
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('center')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')

<div class="row">
    <div class="col-4">
        <button type="button" class="btn btn-block btn-outline-success btn-lg" onclick="window.location.href ='{{ route('writeBlog') }}'">نوشته جدید</button>

    </div>
    <div class="col-4">
        <button type="button" class="btn btn-block btn-outline-warning btn-lg" onclick="window.location.href ='{{ route('indexBlog') }}'">ویرایش نوشته</button>

    </div>

    <div class="col-4">
        <button type="button" class="btn btn-block btn-outline-danger btn-lg" onclick="window.location.href ='{{ route('deleteBlog') }}'">حذف نوشته</button>

    </div>


</div>

@endsection
