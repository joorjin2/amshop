@extends('manager.layouts.master')
@section('title')
    ویرایش پست
@endsection


@section('style')
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">

    <style>
        .blogForm .mainImageBlog {
            width: 60%;
            height: 300px;
            margin-right: 20%;
        }

        .blogForm .mainImageBlog label {
            width: 100%;
            height: 300px;

        }

        .blogForm .mainImageBlog label input {
            display: none;
        }

        .blogForm .mainImageBlog label img {
            width: 100%;
            height: 300px;
        }

        .pwt-btn-calendar {
            display: none !important;
        }

    </style>
@endsection



@section('script')
    <script src="{{ asset('LTE_master/plugins/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: '#mytextarea',
            plugins: 'directionality,image',
            language: 'fa',
            extended_valid_elements: 'span',
            verify_html: true,
            height: 300,
            plugins: ['link emoticons image'],
            image_advtab: true,
            image_title: true,
            image_caption: true,
            relative_urls: false,


        });
    </script>


    {{-- مدیریت تصویر --}}
    <script>
        $(document).ready(function() {



            $(".imgblog").change(function() {
                //Get count of selected files
                var numimg = $(this).attr("num");
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var img = e.target.result;
                        var idimg = "#imgshow" + numimg;
                        $(idimg).attr("src", img);
                    }
                    reader.readAsDataURL($(this)[0].files[i]);
                }
            });
        });
    </script>
@endsection



@section('center')
    {{-- <h1>متن بلاگ</h1> --}}

{{-- {{ Success::get() }} --}}

    <br>
    <form method="post" class="blogForm" enctype="multipart/form-data" action='{{ route("updateBlog", $blog->id) }}'>
        @csrf
        @method('PUT')


        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach ($errors->all() as $item)
                    {{ $item }}.<br>
                @endforeach
            </div>
        @endif


        <div class=" form-label mainImageBlog">
            <label for="img1">
                <input type="file" num="1" name="main_image" id="img1" class="imgblog form-control">
                <img id="imgshow1" src="{{ asset('upload/blog_image/'.$blog->main_image) }}" alt="{{ $blog->title }}">
            </label>
        </div>


        {{-- <label class="form-label mainImageBlog" for="customFile" style="width: 60%;margin-right: 30%;">
            <img style="width: 60%;" src="{{ asset('LTE_master/dist/img/upload.png') }}" alt="">
            <input name="image" type="file" style="display: none" class="form-control" id="customFile" />
        </label> --}}


        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">موضوع نوشته</span>
            </div>
            <input name="title" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3"
                maxlength="255" value="{{ $blog->title }}">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">توضیحات</span>
            </div>
            <input name="description" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3"
                maxlength="255" value="{{ $blog->description }}">
        </div>


        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">تاریخ انتشار نوشته</span>
            </div>
            <input name="started_at" type="text" class="form-control" id="persianDatapicker" aria-describedby="basic-addon3" value="{{ $blog->started_at }}">

        </div>


        <textarea id="mytextarea" name="text" placeholder="متن بلاگ">{{ $blog->text }}</textarea>
        <br>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">تگ ها</span>
            </div>
            <input name="tag" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3"
                maxlength="255" placeholder="تگ ها با * از هم جدا میشوند" value="{{ $blog->tag }}">
        </div>


        <br>
        <br>

        <button type="submit" class="btn btn-block btn-outline-success btn-sm">ذخیره و انتشار</button>

        <br>
        <br>
        <br>
    </form>
@endsection
