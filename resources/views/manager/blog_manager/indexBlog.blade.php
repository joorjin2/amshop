@extends('manager.layouts.master')
@section('title')
    لیست پست ها
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset ('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('center')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
    <div class="col-9 text-center mx-auto pt-5">
            <table class="table table-dark table-striped">
                <thead>
                <tr>
                    <th scope="col" class="col-1">شماره</th>
                    <th scope="col" class="col-2">تصویر</th>
                    <th scope="col" class="col-2">عنوان</th>
                    <th scope="col" class="col-1">عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($blog as $item)
                    <tr>
                        <td class="align-middle pr-2">{{ $item->id }}</td>
                        <td class="align-middle"><img src="{{ asset('upload/blog_image/'.$item->main_image) }}" alt="{{ $item->title }}" class="col-12 h-auto"></td>
                        <td class="align-middle col-7">{{ $item->title }}</td>
                        <td class="align-middle col-12">
                            <a class="btn btn-primary" href="/manager/editBlog/{{ $item->id }}"><i class="fa fa-pencil-square-o text-white fa-lg"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>{{ $blog->links() }}</div>
    </div>
@endsection
