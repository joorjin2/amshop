@extends('manager.layouts.master')
@section('title')
    حذف پست
@endsection


@section('style')
<style>
.photo{
    border-radius: 4%;
}
</style>
@endsection



@section('script')
@isset($_GET['id'])
    <script>
        alert('با موفقیت حذف شد');
        window.location.href = "/manager/deleteBlog";
    </script>
@endisset
<script>
        function del(id){

        if (    confirm("آیا از حذف این خبر اطمینان دارید ؟ | این یک عملیات غیر قابل بازگشت میباشد")){
            window.location="/manager/deleteBlog?id="+id;
        }
        else{
            alert("حذف پست لغو شد");
        }
    }
</script>
@endsection



@section('center')


@foreach ($blog as $item)


<div class="info-box row box">
    <div  class="container-fluid top">
        <div class="card card-danger card-outline">

        </div>
    </div>
    <div class="row">
        <img class="col-3 photo" src="{{ asset($item->main_image) }}" alt="">

        <div class="info-box-content col-9">
            <p>{{ $item->title }}</p>
        </div>
    </div>

    <button type="button" onclick="del({{$item->id}})"  class="btn btn-block  btn-outline-danger col-3" style="margin-right: 75%" > حذف پست</button>
    <!-- /.info-box-content -->
  </div>


  @endforeach

@endsection

