{{-- <link rel="stylesheet" href ="bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/navBar.css">
<link rel="stylesheet" href="css/mainSlider.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/blog-list.css">
<link rel="stylesheet" href="css/style.css"> --}}

@extends('layouts.master')

@section('title')
    نمایش پست
@endsection




@section('style')
    <link rel="stylesheet" href="{{ asset('css/blog-list.css') }}">
@endsection




@section('center')
    <!-- ----------header Slider start---------- -->
    <section class="ftco-section">
        <br> <br> <br> <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="featured-carousel-header owl-carousel">


                        @foreach ($slider as $item)
                            <div class="item" onclick='window.location.assign("{{ route("blogSingle",$item->id) }}")'>
                                <div class="work">
                                    <div class="img d-flex align-items-end justify-content-center"
                                        style="background-image: url('{{asset('upload/blog_image').'/'.$item->main_image }}');">
                                        <div class="text w-100">
                                            <span class="catt" style="color: #fff;"></span>
                                            <h3><a href="#">{{ $item->title }}</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ----------end header Slider----------  -->

    {{-- <a href="">fdfgfgf gfg</a> --}}



    <!-- ----------square-1 start---------- -->

    <div class="square square-1 row ">

        <div class="big-post col-lg-8" style="background-image: url('{{asset('upload/blog_image').'/'.$main[0]->main_image }}');">
            <h2>{{ $main[0]->title }}</h2>
        </div>

        <div class="col-lg-4">
            <div class="medium-post col-lg-12" style="background-image: url('{{asset('upload/blog_image').'/'.$main[1]->main_image }}');">
                <h2>{{ $main[1]->title }}</h2>
            </div>
            <div class="col-lg-12 ">
                <div class="small-post col-lg-6 float-start" style="background-image: url('{{asset('upload/blog_image').'/'.$main[2]->main_image }}');">
                <h2>{{ $main[2]->title }}</h2>

                </div>
                <div class="small-post col-lg-6 float-end" style="background-image: url('{{asset('upload/blog_image').'/'.$main[3]->main_image }}');">
                    <h2>{{ $main[3]->title }}</h2>

                </div>
            </div>

        </div>

    </div>

    <!-- ----------square-1 Slider----------  -->
    <br>

    <!-- ----------square-2 start---------- -->

    <div class="square square-2 row ">

        <div class="col-lg-4">
            <div class="medium-post col-lg-12" style="background-image: url('{{asset('upload/blog_image').'/'.$main[4]->main_image }}');">
                <h2>{{ $main[4]->title }}</h2>

            </div>
            <div class="col-lg-12 ">
                <div class="small-post col-lg-6 float-start" style="background-image: url('{{asset('upload/blog_image').'/'.$main[5]->main_image }}');">
                    <h2>{{ $main[5]->title }}</h2>

                </div>
                <div class="small-post col-lg-6 float-end" style="background-image: url('{{asset('upload/blog_image').'/'.$main[6]->main_image }}');">
                    <h2>{{ $main[6]->title }}</h2>

                </div>
            </div>

        </div>


        <div class="big-post col-lg-8" style="background-image: url('{{asset('upload/blog_image').'/'.$main[7]->main_image }}');">
            <h2>{{ $main[7]->title }}</h2>

        </div>

    </div>

    <!-- ----------square-2 Slider----------  -->
    <br>



    <!-- ----------square-3 start---------- -->

    <div class="square square-3 row ">

        <div class="col-lg-4" style="height: 300px;">
            <div class="medium-post col-lg-12" style="background-image: url('{{asset('upload/blog_image').'/'.$main[8]->main_image }}');height: 100%;">
                <h2>{{ $main[8]->title }}</h2>

            </div>

        </div>
        <div class="col-lg-4" style="height: 300px;">
            <div class="medium-post col-lg-12" style="background-image: url('{{asset('upload/blog_image').'/'.$main[9]->main_image }}');height: 100%;">
                <h2>{{ $main[9]->title }}</h2>

            </div>

        </div>
        <div class="col-lg-4" style="height: 300px;">
            <div class="medium-post col-lg-12" style="background-image: url('{{asset('upload/blog_image').'/'.$main[10]->main_image }}'); height: 100%;">
                <h2>{{ $main[10]->title }}</h2>

            </div>
        </div>

    </div>

    <!-- ----------square-3 Slider----------  -->
    <br2 />
@endsection
