@extends('layouts.master')

@section('title')
  نمایش پست

@endsection




@section('style')
    <link rel="stylesheet" href="{{ asset ('css/blog-singel.css') }}">
@endsection




@section('center')

@foreach ($blog as $item )


<div class="row">
    <div class="col-lg-12 bg-white">
        <div class="col-lg-12 bg-img" style="margin: 0 auto; background-image: url('{{asset('upload/blog_image').'/'.$item->main_image }}');">
            <div class="titr">
                <div class="title">
                    <h3>{{ $item->title }}</h3>
                </div>

                <div class="triangle"></div>
              </div>
        </div>
        <div class="text col-11" >
            {!! $item->text !!}
        </div>
@endforeach
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
@endsection
