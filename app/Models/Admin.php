<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\MenuGroupAccess;

class Admin extends Model
{
    use HasFactory;

    public function access($user_id = null)
    {

        $user_rank = session()->get('user_info')->rank;
        $user_id = ($user_id == null) ? session()->get('user_info')->id : $user_id;

//        $access = DB::table('menu_group_accesses')
//            ->leftJoin('menus', 'menus.id', '=', 'menu_group_accesses.menu_id')
//            ->where('menu_group_accesses.rank', '=', $user_rank)
//            ->get();

        $group_access = MenuGroupAccess::leftJoin('menus', 'menus.id', '=', 'menu_group_accesses.menu_id')
            ->where('menu_group_accesses.rank', '=', $user_rank)
            ->get();

        $user_access = MenuAccess::leftJoin('menus', 'menus.id', '=', 'menu_accesses.menu_id')
            ->where('menu_accesses.user_id', '=', $user_id)
            ->get();

        $addUserAccess = $user_access->where('status', 'add');
        $deleteUserAccess = $user_access->where('status', 'delete')->pluck('menu_id');

        // add user access
        $user_access = $group_access->merge($addUserAccess);
        // delete user access
        $access = $user_access->whereNotIn('menu_id',$deleteUserAccess);

        return $access;
    }
}
