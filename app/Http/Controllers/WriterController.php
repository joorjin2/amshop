<?php

namespace App\Http\Controllers;

use App\lib\Err;
use App\Models\Admin;
use App\Models\Token;
use CreateWritersTable;
// use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Else_;
use Illuminate\Contracts\Validation;

class WriterController extends Controller
{
    public function Register(Request $request)
    {

        if ($request->isMethod('post')) {

            $validated = $request->validate([
                'name' => 'bail|required',
                'last_name' => 'bail|required|',
                'phone' => 'bail|required|unique:admins|regex:/(09)[0-9]{9}/',
                'email' => 'bail|required|email|unique:admins',
                'password' => 'bail|required|min:8|max:20|',
            ]);


            $admin = new Admin();
            $admin->first_name = $request->name;
            $admin->last_name = $request->last_name;
            $admin->phone = $request->phone;
            $admin->email = $request->email;
            $admin->password = md5($request->password);
            $admin->save();
            echo ('ثبت نام با موفقیت انجام شد');
        } else {
            return view('writers.Register');
        }

    }
    public function login(request $request , MessageBag $message_bag)
    {

        if (Err::show('noAcsess')) {
            $message_bag->add('noAcsess', Err::show('noAcsess'));
            return redirect()->route('writer-login')->withErrors($message_bag);
        }

        if($request->isMethod('post')) {




            $validated = $request->validate([
                'email' => 'bail|required|email|',
                'password' => 'bail|required|min:8|max:30|',
            ]);

            $Writer = Admin::
            where('email',$request->email)
                ->where('password',md5($request->password))
                ->get();

            if (!$Writer->count()) {
                $message_bag->add('password', 'پسورد اشتباه است');
                return redirect()->route('writer-login')->withErrors($message_bag);
            }

            $Writer_id = $Writer[0]['id'];




            if (!Token::where('user_id',$Writer_id)->count()) {
                $random = random();
                $token = new Token();
                $token->model = 'Writer';
                $token->user_id = $Writer_id;
                $token->token = $random;
                $token->save();

                setcookie('TOKEN',$random , time() + (86400 * 30),'/');

            }else{

                $token = Token::where('user_id',$Writer_id)->get();
                $token=$token[0]->token;
                setcookie('TOKEN',$token , time() + (86400 * 30),'/');

            }




            return redirect()->route('manager-index');

        } else {
            return view('writers.login');
        }
    }


    public function logout(){
        session()->forget('user_info');
        return redirect(route('writer-login'));
    }
}
