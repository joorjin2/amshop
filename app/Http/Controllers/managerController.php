<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Menu;
use App\Models\MenuGroupAccess;

use App\Http\Middleware\AdminValidation;
use App\Models\MenuAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class managerController extends Controller
{
    public function index()
    {
        return view('manager.index');
    }

    public function blog()
    {
        return view('manager.blog_manager.blogMenu');
    }

    public function product()
    {
        return view('manager.blog_manager.productMenue');
    }

    public function accesses()
    {
        return view('manager.accesses');
    }

    public function getMenus(Request $request)
    {

        if (session()->get('user_info')->rank != 'admin') {
            abort(403);
        }

        $request->validate([
            'rank' => 'string|between:2,60',
            'user' => 'integer|between:1,5'
        ]);

        // get group access menus
        if (isset($request->rank)) {

            // menus that the rank has access
            $access = MenuGroupAccess::leftJoin('menus', 'menus.id', '=', 'menu_group_accesses.menu_id')
                ->where('menu_group_accesses.rank', '=', $request->rank)
                ->get();

            // all menus
            $menus = Menu::all();

            // get menus that the rank has not access
            $notAccesses = $menus->whereNotIn('id', $access->pluck('menu_id'));

            return array("access" => $access, "notAccess" => $notAccesses);

        }

        // get user access menu
        if (isset($request->user)) {

            // menus that the user has access
            $access = session()->get('user_info')->access($request->user);

            // all menus
            $menus = Menu::all();

            // get menus that the rank has not access
            $notAccesses = $menus->whereNotIn('id', $access->pluck('menu_id'));

            return array("access" => $access, "notAccess" => $notAccesses);

        }

        return abort(403);
    }

    public function groupAccess(Request $request)
    {

        $ranks = Admin::select('rank')->distinct()->get();

        if ($request->isMethod('post')) {

            $request->validate([
                'menu' => 'required',
                'rank' => 'required',
                'access' => 'required'
            ]);

            if ($request->access == 'add') {

                $menuAccessGroup = new MenuGroupAccess;
                $menuAccessGroup->rank = $request->rank;
                $menuAccessGroup->menu_id = $request->menu;
                $menuAccessGroup->save();

            }
            if ($request->access == 'delete') {

                MenuGroupAccess::where('rank', $request->rank)->where('menu_id', $request->menu)->delete();

            }

            return redirect()->back()->with('ok', 'عملیات موفقیت امیز بود');
        }

        return view('manager.groupAccess', compact('ranks'));

    }

    public function userAccess(Request $request)
    {
        $admins = Admin::all();

        if ($request->isMethod('post')) {

            $request->validate([
                'menu' => 'required',
                'user' => 'required',
                'access' => 'required'
            ]);

            if ($request->access == 'add') {

                $userMenuAccess = new MenuAccess;
                $userMenuAccess->user_id = $request->user;
                $userMenuAccess->menu_id = $request->menu;
                $userMenuAccess->status = $request->access;
                $userMenuAccess->save();

                MenuAccess::where('user_id', $request->user)->where('menu_id', $request->menu)->where('status', 'delete')->delete();

            }
            if ($request->access == 'delete') {

                $userMenuAccess = new MenuAccess;
                $userMenuAccess->user_id = $request->user;
                $userMenuAccess->menu_id = $request->menu;
                $userMenuAccess->status = $request->access;
                $userMenuAccess->save();

            }

            return redirect()->back()->with('ok', 'عملیات موفقیت امیز بود');
        }

        return view('manager.userAccess', compact('admins'));
    }
}
//
