<?php

namespace App\Http\Controllers;

use App\lib\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Product;
use App\Models\ProductPicture;
use App\Models\ProductPrice;

class ProductController extends Controller
{
    public function addProduct(Request $request)
    {
        if ($request->isMethod('post')) {

            $request->validate([
                'productTitle' => 'required',
                'productBrand' => 'required',
                'productCategory' => 'required',
                'productDescription' => 'required',
                'productKeywords' => 'required',
                'productImage' => 'required|max:4',
                'productImage.*' => 'mimes:png,jpg,jpeg',
                'productPrice' => 'required|integer'
            ]);

            $product = new product;
            $product->product_title = $request->productTitle;
            $product->product_brand = $request->productBrand;
            $product->product_category = $request->productCategory;
            $product->product_description = $request->productDescription;
            $product->product_keywords = $request->productKeywords;
            $product->save();

            $product_price = new productPrice;
            $product_price->product_id = $product->id;
            $product_price->price = $request->productPrice;
            $product_price->save();

            // upload and insert
            $images = $request->file('productImage');

            foreach ($images as $image) {

                $image_name = date("Ymdhis") . $image->getClientOriginalName();
                uploadFile($image, 'upload/product_image');

                $product_picture = new productPicture;
                $product_picture->product_id = $product->id;
                $product_picture->image_name = $image_name;
                $product_picture->status = '';
                $product_picture->save();

            }

            $ok = 'محصول با موفقیت افزوده شد';
            $categories = product::select('product_category')->distinct()->get();

            return view('manager.addProduct', compact('categories', 'ok'));
        }

        $categories = product::select('product_category')->distinct()->get();
        return view('manager.addProduct', compact('categories'));

    }
}
