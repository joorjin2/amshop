<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    public function addMenu(Request $request){

        if ($request->isMethod('post')){

            $request->validate([
                'menuTitle' => 'required',
                'menuIcon' => 'required',
                'underMenuId' => 'required',
                'menuName' => 'required'
            ]);

            $menus = new Menu;
            $menus->title = $request->menuTitle;
            $menus->icon = $request->menuIcon;
            $menus->under_menu_id = $request->underMenuId;
            $menus->name = $request->menuName;
            $menus->save();

            $ok = 'منو با موفقیت افزوده شد';
            return view('manager.addMenu',compact('ok'));
        }

        return view('manager.addMenu');
    }
}
