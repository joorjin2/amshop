<?php

namespace App\Http\Controllers;

use App\lib\Success;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\BlogRequest;

class BlogController extends Controller
{
    public function blogSingle($id)
    {
        visitor()->visit();
        $blog = Blog::where('id', $id)->where('status', 'display')->get();
        $blog_num = $blog->count();
        if (!$blog_num) {
            abort(404);
        }
        return view('blog.blog-single', compact('blog'));
    }


    public function blogList()
    {
        visitor()->visit();

        $slider = Blog::orderby('id', 'DESC')
            ->where('status', 'display')
            ->limit(5)
            ->get();
        $main = Blog::orderby('id','DESC')
        ->where('status', 'display')
        ->limit(11)
        ->get();

        return view('blog.blog-list', compact('slider','main'));
    }




    public function writeBlog(Request $request){

        if ($request->isMethod('post')) {

            $request->validate([
                'title'=>'required|max:250',
                'text'=>'required',
                'image'=>'required|image'
            ]);

            Success::set('ok');

            // upload
            $image_name = uploadFile($request->image, 'upload/blog_image');
            $time = jalaliToGregorian($request->time);


            $blog = new Blog();
            $blog->writer_id= Session::get('user_info')['id'];
            $blog->status='waiting_confirmation';
            $blog->title=$request->title;
            $blog->description=$request->description;
            $blog->text=$request->text;
            $blog->small_image=$image_name;
            $blog->main_image=$image_name;
            $blog->tag=$request->tag;
            $blog->started_at=$time;
            $blog->created_at=$time;
            $blog->updated_at=$time;
            $blog->save();

            alert()->success('!Created');
            return redirect()->route('blog-manager');
        }
        return view('manager.blog_manager.writeBlog');
    }


    public function indexBlog()
    {
        $user_id = session()->get('user_info')->id;
        $blog = Blog::where('writer_id','=',$user_id)->orderBy('id', 'desc')->paginate(10);
        return view('manager.blog_manager.indexBlog', compact('blog'));
    }


    public function editBlog($id)
    {
        $blog = Blog::where('id', '=', $id)->first();
        return view('manager.blog_manager.editBlog', compact('blog'));
    }


    public function updateBlog(BlogRequest $request, Blog $blog)
    {
        abort_if(session()->get('user_info')->id != $blog->writer_id, 403);
        $data = $request->validated();


        if($blog->started_at != $data['started_at'])
        {
            $data['started_at'] = jalaliToGregorian($data['started_at']);
        }
        if(isset($data['main_image']) && isset($blog->main_image))
        {
            deleteFile($blog->main_image);
        }
        if(isset($data['main_image']))
        {
            $data['main_image'] = uploadFile($data['main_image'], 'upload/blog_image');
        }

        $blog->update($data);
        // alert()->success('با موفقیت انجام شد', 'آپدیت نوشته')->persistent('باشه');
        alert()->success('!Updated');
        return redirect()->route('indexBlog');
    }


    public function deleteBlog()
    {

        if(isset($_GET['id'])){
            Blog::where("id", $_GET['id'])->update([
                "status" => 'delete',
                "deleted_at" => now()
            ]);


        }
        $blog = Blog::where("status",'display')->where("writer_id", Session::get('user_info')['id'])->get();

        return view('manager.blog_manager.deleteBlog',compact('blog'));




    }
}


