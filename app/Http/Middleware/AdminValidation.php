<?php

namespace App\Http\Middleware;

use App\lib\Err;
use App\Models\Admin;
use App\Models\Token;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Validated;

class AdminValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // dd($_COOKIE['TOKEN']);
        if (isset($_COOKIE['TOKEN'])) {
            $token = Token::where('token',$_COOKIE['TOKEN'])->get();
            if(!$token->count()){
                // dd(route('writer-login'));


                Err::set('writer-login','noAcsess','برای دسترسی به این صفحه ابتدا وارد شوید');

            }
            $user_id=$token[0]->user_id;
            // $model=$token[0]->model;
            // $model ='\App\Models\\'.$model;
            // $user_info = $model::where('id',$user_id)->get();


            $user_info = Admin::where('id',$user_id)->get();


            Session::put('user_info', $user_info[0]);

            // check for user access
            $user_accesses = session()->get('user_info')->access();

            $isAccessed = false;

            foreach ($user_accesses as $menu) {
                if ($request->url() == route($menu->name)) {
                    $isAccessed = true;
                }
            }
            if (!$isAccessed) {
                abort(403);
            }

            return $next($request);
        }else{

            // dd(route('writer-login'));
            Err::set('writer-login','noAcsess','برای دسترسی به این صفحه ابتدا وارد شوید');

        }
    }
}
