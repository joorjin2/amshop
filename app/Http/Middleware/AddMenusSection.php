<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AddMenusSection
{
    public function handle(Request $request, Closure $next)
    {

        $menu = Menu::all();


        Session::put('menu', $menu);

        return $next($request);
    }
}
