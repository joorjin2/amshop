<?php

namespace App\Console\Commands;

use App\Models\Blog;
use App\Models\Menu;
use App\Models\UserValidation;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;



class dbadd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:testdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run db test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->line("Some text");
        // $this->info("Hey, watch this !");
        // $this->comment("Just a comment passing by");
        // $this->question("Why did you do that?");
        // $this->error("Ops, that should not happen.");
        $this->comment("start..");




// command
        $this->comment("Preliminary review ...");
        $this->comment("rollback");
        Artisan::call('migrate:rollback');
        Artisan::call('migrate:rollback');
        Artisan::call('migrate:rollback');
        $this->comment("migrate");
        Artisan::call('migrate');

//query
        $this->question("run query test");

//user table
        $this->comment("add data in  Admin tabele➕");
        $writer = new Admin;
        $writer->status = 'active';
        $writer->first_name = 'علی';
        $writer->last_name = 'مجد یزدی';
        $writer->email = 'mz.am.busines@gmail.com';
        $writer->email_confirm = NULL;
        $writer->password = md5(12345678);
        $writer->phone = Null;
        $writer->telegram_id = 'test';
        $writer->telegram_number_id = 125489365;
        $writer->telegram_confirm = Null;
        $writer->confirmation_manager_id = 5;
        $writer->started_at = now();
        $writer->latest_activists_at = now();
        $writer->deleted_at = Null;
        $writer->save();


        $this->comment("add data in  Admin tabele➕");
        $writer = new Admin;
        $writer->status = 'active';
        $writer->first_name = 'محمد';
        $writer->last_name = 'عظیمی';
        $writer->email = 'azimi@gmail.com';
        $writer->email_confirm = NULL;
        $writer->password = md5(12345678);
        $writer->phone = Null;
        $writer->telegram_id = 'test';
        $writer->telegram_number_id = 125489365;
        $writer->telegram_confirm = Null;
        $writer->confirmation_manager_id = 5;
        $writer->started_at = now();
        $writer->latest_activists_at = now();
        $writer->deleted_at = Null;
        $writer->save();


        $this->comment("add data in  UserValidation tabele➕");
        $seed = str_split(
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            .'abcdefghijklmnopqrstuvwxyz'
            .'*/-!@#$%^&*+=-_|\)('
            .'0123456789');
            shuffle($seed);
            $rand = '';
            foreach (array_rand($seed, 80) as $k) $rand .= $seed[$k];

        $UserValidation = new UserValidation;
        $UserValidation->user_id = 1;
        $UserValidation->tabele_name="Admin";
        $UserValidation->token="$rand";
        $UserValidation->ended_at=Carbon::now()->addDays(30);
        $UserValidation->save();







        // $start = new Awards;
        // $start->name = '-0/6 % تخفیف';
        // $start->delivery_in_time = 20160;
        // $start->time_open=Carbon::now();
        // $start->number = 6;
        // $start->number_left= 6;
        // $start->type = 'discount';
        // $start->save();



        for ($i=1; $i <= 15 ; $i++) {
            $this->comment("add  data  in  Blog tabele <{$i}>  ➕");

            $blog = new Blog();
            $blog->writer_id= rand(1,2);
            $blog->status='display';
            $blog->title="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ";
            $blog->description="لورم ی اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.";
            $blog->text='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.';
            $blog->small_image="big{$i}.jpg";
            $blog->main_image="big{$i}.jpg";
            $blog->save();
        }




        $this->comment("add data in  menus tabele /➕");
        $menu = new Menu;
        $menu->under_menu_id=null;
        $menu->icon="bi bi-speedometer2";
        $menu->title="داشبورد";
        $menu->name="manager-index";
        $menu->save();

        $this->comment("add data in  menus tabele blog➕");
        $menu = new Menu;
        $menu->under_menu_id=null;
        $menu->icon="bi bi-file-earmark-post";
        $menu->title="بلاگ";
        $menu->name="blog-manager";
        $menu->save();


















        $this->info("v=1.2.0 ★");

        $this->info("done successfully!✔️");





    }
}
