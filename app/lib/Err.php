<?php
namespace App\lib;

class Err{


    public static function set($route_name,$error_name,$error_text){


        header("Location:".route($route_name).'?'.$error_name.'='.$error_text);
        die();

    }

    public static function show($error_name){

        if (isset($_GET[$error_name])) {

            return $_GET[$error_name];
        }else{
            return 0;
        }

    }
}

