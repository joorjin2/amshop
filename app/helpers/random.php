<?php

    function random(){
        $seed = str_split(
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            .'abcdefghijklmnopqrstuvwxyz'
            .'*/-!@#$%^&*+=-_|\)('
            .'0123456789');
            shuffle($seed);
            $rand = '';
            foreach (array_rand($seed, 80) as $k) $rand .= $seed[$k];

            return $rand;
    }

    function carbonToPersian($carbonDate)
    {
        return \Morilog\Jalali\Jalalian::fromCarbon($carbonDate);
    }


