<?php
function jalaliToGregorian($dateTime)
{
    $v = \Hekmatinasser\Verta\Verta::parse($dateTime);
    $c = \Illuminate\Support\Carbon::instance($v->datetime());
    return $c;
}
