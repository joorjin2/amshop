<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWritersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writers', function (Blueprint $table) {
            $table->id()->comment('شناسه یونیک');
            $table->string('rank')->default('writer');
            $table->integer('confirmation_manager_id')->nullable()->comment('شناسه یونیک مدیر تایید کننده');
            $table->enum('status', ['active','block','waiting_confirmation'])->default('waiting_confirmation')->comment('وضعیت نویسنده {فعال،بلاک شده،منتظر تایید}');
            $table->string('first_name')->comment('نام کوچک ');
            $table->string('last_name')->comment('نام خانوادگی');
            $table->string('email')->comment('ایمیل');
            $table->string('email_confirm')->nullable()->comment('وضعیت تایید ایمیل');
            $table->string('password')->comment('گذرواژه');
            $table->string('phone')->nullable()->comment('شماره تلفن');
            $table->string('telegram_id')->nullable()->comment('آیدی تلگرام');
            $table->integer('telegram_number_id')->nullable()->comment('آیدی عددی تلگرام');
            $table->string('telegram_confirm')->nullable()->comment(' وضعیت تایید تلگرام');
            $table->integer('score')->default(50)->comment(' نمره نویسنده(نمره پیش فرض 50 میباشد)');
            $table->timestamp('started_at')->nullable()->comment('زمان شروع به کار');
            $table->timestamp('latest_activists_at')->nullable()->comment(' زمان آخرین فعالیت');
            $table->timestamp('deleted_at')->nullable()->comment(' زمان پاک شدن کاربر (وقتی کاربر پاک شود این جا ثبت میشود)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writers');
    }
}
