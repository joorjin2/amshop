<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\managerController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\WriterController;
use App\Http\Middleware\AddMenusSection;
use App\Http\Middleware\writerValidation;
use App\Models\Blog;
use Illuminate\Support\Facades\Route;
use League\CommonMark\Parser\Block\BlockContinue;




Route::get('/', function () {

    return view('blog-list');
});
Route::any('blog-list',[BlogController::class,'blogList']);

Route::get('test1',function(){
    return view('test');
});


Route::any('test',[TestController::class,'test']);




Route::controller(WriterController::class)->group(function () {
    Route::prefix('writer')->group(function () {

        Route::any('register','Register');
        Route::any('login','login')->name('writer-login');
    });
//end writer url
});
//end writer controller



Route::prefix('blog')
->group(function () {
    Route::any('blog-single/{id}',[BlogController::class,'blogSingle']);
});
//end blog controller




Route::middleware([AddMenusSection::class,writerValidation::class])
->prefix('manager')
->group(function(){
    Route::view('base', 'manager.base');

    Route::any('/',[managerController::class,'index'])->name('manager-index');
    Route::any('blog',[managerController::class,'blog'])->name('blog-manager');
    Route::any('writeBlog',[BlogController::class,'writeBlog'])->name('writeBlog');
    Route::any('editBlog',[BlogController::class, 'editBlog'])->name('editBlog');
    Route::any('deleteBlog',[blogcontroller::class,'deleteBlog'])->name('deleteBlog');
});




