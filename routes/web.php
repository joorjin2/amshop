<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\managerController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\WriterController;
use App\Http\Middleware\AddMenusSection;
use App\Http\Middleware\AdminValidation;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\ProductController;

use App\Models\Blog;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use League\CommonMark\Parser\Block\BlockContinue;




Route::get('/', function(){
    return redirect()->route('blogList');
});
Route::any('blog-list',[BlogController::class,'blogList'])->name('blogList');

Route::get('test1',function(){
    return view('test');
});


Route::any('test',[TestController::class,'test']);




Route::controller(WriterController::class)->group(function () {
    Route::prefix('writer')->group(function () {

        Route::any('register','Register');
        Route::any('login','login')->name('writer-login');
        Route::any('logout','logout')->name('writer-logout');
    });
//end writer url
});
//end writer controller



Route::prefix('blog')
->group(function () {
    Route::any('blog-single/{id}',[BlogController::class,'blogSingle'])->name('blogSingle');
});
//end blog controller




Route::middleware([AddMenusSection::class,AdminValidation::class])
->prefix('manager')
->group(function(){
    Route::view('base', 'manager.base');

    Route::any('/',[managerController::class,'index'])->name('manager-index');
    Route::any('blog',[managerController::class,'blog'])->name('blog-manager');
    Route::any('product',[managerController::class,'product'])->name('product-manager');
    Route::any('writeBlog',[BlogController::class,'writeBlog'])->name('writeBlog');
    Route::any('indexBlog',[BlogController::class, 'indexBlog'])->name('indexBlog');
    Route::any('editBlog/{blog}',[BlogController::class, 'editBlog'])->name('editBlog');
    Route::put('updateBlog/{blog}',[BlogController::class, 'updateBlog'])->name('updateBlog');
    Route::any('deleteBlog',[blogcontroller::class,'deleteBlog'])->name('deleteBlog');
    Route::any('addMenu',[MenuController::class,'addMenu'])->name('addMenu');
    Route::any('addProduct',[ProductController::class,'addProduct'])->name('addProduct');
    Route::any('accesses',[managerController::class,'accesses'])->name('accesses');
    Route::any('accesses/groupAccess',[managerController::class,'groupAccess'])->name('groupAccess');
    Route::any('accesses/userAccess',[managerController::class,'userAccess'])->name('userAccess');

});

Route::post('getMenus',[managerController::class,'getMenus'])->name('getMenus');

//Route::middleware(AdminValidation::class)
//    ->post('getMenus',[managerController::class,'getMenus'])
//    ->name('getMenus');

Route::view('testOne', 'manager.testOne');


Route::any('product-manager');
